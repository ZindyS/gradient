package com.example.gradient

data class ColorData(
    val R : Int,
    val G : Int,
    val B : Int
)
