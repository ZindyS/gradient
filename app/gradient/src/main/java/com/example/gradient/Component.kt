package com.example.gradient

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import java.lang.Integer.min
import java.lang.Math.sqrt

@SuppressLint("ClickableViewAccessibility")
class Component @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {
    private lateinit var colors : List<ColorData>
    private var currentColor = ColorData(0, 0, 0)
    private var actradius = 0f
    private var radius = 0f
    private var listener : OnColorChangedListener? = null
    private var indicator = Indicator(context)

    init {
        colors = listOf(ColorData(0, 0, 0))
        this.setOnTouchListener { view, motionEvent ->
            val x = motionEvent.x-actradius
            val y = motionEvent.y-actradius
            val dist = sqrt(x*x*1.0 + y*y*1.0)
            Log.d("Errror", "${motionEvent.x} ${motionEvent.y}, $radius $dist")
            if (dist<radius) {
                indicator.x = motionEvent.x
                indicator.y = motionEvent.y
                if (colors.size != 1) {
                    val shag = radius / (colors.size - 1)
                    val i = colors.size - 1 - (dist / shag).toInt()
                    val j = dist % shag
                    Log.d("ErrrorAboba", "$shag $dist, $i $j")
                    currentColor = ColorData(
                        (colors[i].R + (colors[i - 1].R - colors[i].R) * j / shag).toInt(),
                        (colors[i].G + (colors[i - 1].G - colors[i].G) * j / shag).toInt(),
                        (colors[i].B + (colors[i - 1].B - colors[i].B) * j / shag).toInt()
                    )
                } else {
                    currentColor = colors[0]
                }

                if (listener != null) {
                    listener!!.update(Color.rgb(currentColor.R, currentColor.G, currentColor.B))
                }
            }
            return@setOnTouchListener true
        }
    }

    fun setColors(colors : List<ColorData>) {
        this.colors = colors
        if (colors.isEmpty()) {
            this.colors = listOf(ColorData(0, 0, 0))
        }
    }

    fun addListener(listener : OnColorChangedListener) {
        this.listener = listener
    }

    override fun onDraw(canvas: Canvas?) {
        indicator = Indicator(context)
        indicator.setBackgroundResource(R.drawable.selector)
        val circle = Paint()
        actradius = (min(this.width, this.height)/2).toFloat()
        radius = actradius*0.8f
        circle.color = Color.rgb(240, 240, 240)
        canvas!!.drawCircle(actradius, actradius, actradius, circle)
        if (colors.size!=1) {
            val shag = radius / (colors.size - 1)
            var now = radius
            for (i in 0..colors.size-2) {
                for (j in 0..shag.toInt()) {
                    circle.color = Color.rgb(
                        (colors[i].R + (colors[i+1].R - colors[i].R) * j / shag).toInt(),
                        (colors[i].G + (colors[i+1].G - colors[i].G) * j / shag).toInt(),
                        (colors[i].B + (colors[i+1].B - colors[i].B) * j / shag).toInt(),
                    )
                    canvas.drawCircle(actradius, actradius, now - j, circle)
                }
                now -= shag
            }
        } else {
            circle.color = Color.rgb(colors[0].R, colors[0].G, colors[0].B)
            canvas.drawCircle(actradius, actradius, radius, circle)
        }
        super.onDraw(canvas)
    }

    interface OnColorChangedListener {
        fun update(currentColor : Int) {}
    }

    class Indicator @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : View(context, attrs, defStyleAttr) {
        override fun onFinishInflate() {
            this.setBackgroundResource(R.drawable.selector)
            this.setBackgroundColor(Color.GREEN)
            this.x=500f
            this.y=500f
            super.onFinishInflate()
        }
    }
}