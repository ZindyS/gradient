package com.example.module

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.gradient.ColorData
import com.example.gradient.Component
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        component.setColors(listOf(
            ColorData(100, 100, 100),
            ColorData(100, 100, 250),
            ColorData(0, 250, 0),
            ColorData(100, 100, 100),
            ColorData(250, 100, 250)
        ))
        component.addListener(object : Component.OnColorChangedListener {
            override fun update(currentColor: Int) {
                textView.setBackgroundColor(currentColor)
                super.update(currentColor)
            }
        })
    }
}